# Configure the AWS provider
provider "aws" {
  access_key = "ACCESS_KEY"
  secret_key = "SECRET_KEY"
  region     = "us-east-1"
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_db_instance" "dbprod" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7.32"
  instance_class       = "db.t2.micro"
  name                 = "espocrm"
  username             = "espocrm"
  password             = "database_password"
  db_subnet_group_name = "lovester_subnet_group"
  vpc_security_group_ids = ["sg-123456"]
  publicly_accessible  = true

  parameter_group_name = "default.mysql5.7"

  backup_retention_period = 0
  skip_final_snapshot     = true
}

resource "aws_db_subnet_group" "proddb" {
  name        = "lovester_subnet_group"
  description = "Example DB subnet group"

  subnet_ids = ["subnet-123456", "subnet-abcdef"]
}
