# Configure the AWS provider
provider "aws" {
  access_key = "ACCESS_KEY"
  secret_key = "SECRET_KEY"
  region     = "us-east-1"
}

# Create an EC2 instance with a Debian 11 operating system
resource "aws_instance" "lovester" {
  ami           = "ami-0d57e88b77a6b78a9" # Debian 11 AMI
  instance_type = "t2.micro"

  # Add a key pair for SSH access
  key_name      = "KEY_NAME"
  security_groups = ["SECURITY_GROUP_NAME"]

  # Add a tag to identify the instance
  tags = {
    Name = "lovester-instance"
  }
}

#ACCESS_KEY and SECRET_KEY: Your AWS access key and secret key.
#REGION: The region where you want to create the instance.
#AMI_ID: The ID of the AMI that you want to use for the instance.
#KEY_NAME: The name of the key pair that you want to use for SSH access.
#SECURITY_GROUP_NAME: The name of the security group that you want to use for the instance.

